//
//  ArticlesSource.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 16/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation
import RealmSwift

class ArticlesSource {

    static func all(in realm: Realm = try! Realm()) -> Results<Article> {
        return realm
            .objects(Article.self)
            .sorted(byKeyPath: #keyPath(Article.published), ascending: false)
    }

    static func favourites(in realm: Realm = try! Realm(), filteringBy searchQuery: String? = nil) -> Results<Article> {
        var predicates = [NSPredicate(format: "\(#keyPath(Article.isFavourite)) = true")]
        if let searchQuery = searchQuery, !searchQuery.isEmpty {
            predicates.append(NSPredicate(format: "\(#keyPath(Article.headline)) CONTAINS[cd] %@", searchQuery))
        }

        return all().filter(NSCompoundPredicate(andPredicateWithSubpredicates: predicates))
    }

    static func save<S: Sequence>(_ articles: S, in realm: Realm = try! Realm()) where S.Element == Article {
        let favouritedArticleIds = Set(favourites(in: realm).map { $0.id })
        try? realm.write {
            articles.forEach { article in
                article.isFavourite = favouritedArticleIds.contains(article.id)
            }

            realm.add(articles, update: true)
        }
    }

    static func update(_ object: Article, in realm: Realm = try! Realm(), block: (Article) -> Void) {
        try! realm.write {
            block(object)
            realm.add(object, update: true)
        }
    }

}
