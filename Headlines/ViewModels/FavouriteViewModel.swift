//
//  FavouriteViewModel.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 17/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import UIKit

class FavouriteViewModel {

    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .short
        return formatter
    }()

    let headlineText: NSAttributedString
    let formattedPublishedDate: NSAttributedString?
    let imageURL: URL?

    init(article: Article) {
        headlineText = type(of: self).makeHeadlineText(for: article)
        formattedPublishedDate = type(of: self).makeFormattedPublishedDate(for: article)
        imageURL = article.imageURL
    }

    private static func makeHeadlineText(for article: Article) -> NSAttributedString {
        let font = UIFont.named(Constants.UI.semiboldFontName, forTextStyle: .headline)
        return NSAttributedString(string: article.headline, attributes: [.font: font])
    }

    private static func makeFormattedPublishedDate(for article: Article) -> NSAttributedString? {
        guard let formattedDate = article.published.map(dateFormatter.string) else {
            return nil
        }

        let font = UIFont.named(Constants.UI.regularFontName, forTextStyle: .subheadline)
        return NSAttributedString(string: formattedDate, attributes: [.font: font])
    }

}
