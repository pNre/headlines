//
//  ArticleViewModel.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 17/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation
import RealmSwift

class ArticleViewModel {

    let article: Article
    var articleReloaded: (() -> Void)?
    private(set) var headlineText: NSAttributedString!
    private(set) var bodyText: NSAttributedString!
    private(set) var imageURL: URL?
    private(set) var startButtonImage: UIImage?

    private var articleToken: NotificationToken?

    init(article: Article) {
        self.article = article
        self.observeArticle()
        self.reload()
    }

    deinit {
        articleToken?.invalidate()
    }

    private func reload() {
        headlineText = makeHeadlineText()
        bodyText = makeBodyText()
        imageURL = article.imageURL
        startButtonImage = makeStartButtonImage()
        articleReloaded?()
    }

    private func observeArticle() {
        articleToken?.invalidate()
        articleToken = article
            .observe { [weak self] (change) in
                switch change {
                case .change:
                    self?.reload()
                case .error,
                     .deleted:
                    break
                }
            }
    }

    private func makeHeadlineText() -> NSAttributedString? {
        let font = UIFont.named(Constants.UI.boldFontName, forTextStyle: .title1)
        return NSAttributedString(string: article.headline, attributes: [.font: font])
    }

    private func makeBodyText() -> NSAttributedString {
        let font = UIFont.named(Constants.UI.regularFontName, forTextStyle: .body)
        let paragraphSyle = NSMutableParagraphStyle()
        paragraphSyle.lineHeightMultiple = 1.5

        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .paragraphStyle: paragraphSyle
        ]

        return NSAttributedString(string: article.body, attributes: attributes)
    }

    private func makeStartButtonImage() -> UIImage? {
        return UIImage(named: article.isFavourite ? "favourite-on" : "favourite-off")
    }

}
