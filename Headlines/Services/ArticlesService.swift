//
//  ArticlesService.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 16/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Alamofire
import Foundation
import RealmSwift

class ArticlesService {

    private static let APIKey = "enj8pstqu5yat6yesfsdmd39"

    static func fetchArticles(completion: @escaping (([Article]?, Error?) -> Void)) -> DataRequest {
        let url = "https://content.guardianapis.com/search?q=fintech&show-fields=thumbnail,body&api-key=\(APIKey)"
        return Alamofire.request(url, encoding: JSONEncoding.default).responseData { response in
            switch response.result {
            case .success(let data):
                let searchResponse: SearchResponse
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    searchResponse = try decoder.decode(SearchResponse.self, from: data)
                } catch let error {
                    completion(nil, error)
                    return
                }

                completion(searchResponse.response.results, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }

    private struct SearchResponse: Decodable {
        let response: SearchResponseData
    }

    private struct SearchResponseData: Decodable {
        let results: [Article]
    }

}
