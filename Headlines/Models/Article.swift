//
//  Article.swift
//  Headlines
//
//  Created by Joshua Garnham on 09/05/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class Article: Object, Decodable {
    @objc private(set) dynamic var id: String!
    @objc private(set) dynamic var headline: String!
    @objc private(set) dynamic var body: String!
    @objc private(set) dynamic var published: Date?
    @objc dynamic var isFavourite = false
    @objc private dynamic var rawImageURL: String?
    var imageURL: URL? {
        guard let rawImageURL = rawImageURL else { return nil }
        return URL(string: rawImageURL)
    }

    enum CodingKeys: String, CodingKey {
        case id
        case headline = "webTitle"
        case published = "webPublicationDate"
        case fields
    }

    enum FieldsCodingKeys: String, CodingKey {
        case body
        case thumbnail
    }

    required convenience init(from decoder: Decoder) throws {
        self.init()

        let articleContainer = try decoder.container(keyedBy: CodingKeys.self)
        id = try articleContainer.decode(String.self, forKey: .id)
        headline = try articleContainer.decode(String.self, forKey: .headline)
        published = try? articleContainer.decode(Date.self, forKey: .published)

        let fieldsContainer = try articleContainer.nestedContainer(keyedBy: FieldsCodingKeys.self, forKey: .fields)
        body = (try fieldsContainer.decode(String.self, forKey: .body)).strippingTags
        rawImageURL = try? fieldsContainer.decode(String.self, forKey: .thumbnail)
    }

    override class func primaryKey() -> String {
        return #keyPath(id)
    }

    override class func indexedProperties() -> [String] {
        return [#keyPath(isFavourite)]
    }

    func toggleIsFavourite() {
        isFavourite.toggle()
    }
}
