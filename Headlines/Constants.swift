//
//  Constants.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 17/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import UIKit

struct Constants {
    struct UI {
        static let animationDuration: TimeInterval = 0.2
        static let regularFontName = "Baskerville"
        static let semiboldFontName = "Baskerville-SemiBold"
        static let boldFontName = "Baskerville-Bold"
    }
}
