//
//  FavouriteCell.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 16/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import SDWebImage
import UIKit

class FavouriteCell: UITableViewCell {

    static let reuseIdentifier = "FavouriteCell"

    @IBOutlet var articleImageView: UIImageView!
    @IBOutlet var articleHeadlineLabel: UILabel!
    @IBOutlet var articleDateLabel: UILabel!

    var viewModel: FavouriteViewModel? {
        didSet {
            articleImageView.sd_setImage(with: viewModel?.imageURL)
            articleHeadlineLabel.attributedText = viewModel?.headlineText
            articleDateLabel.attributedText = viewModel?.formattedPublishedDate
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        articleImageView.sd_cancelCurrentImageLoad()
    }

}
