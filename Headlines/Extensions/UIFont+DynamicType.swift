//
//  UIFont+DynamicType.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 17/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import UIKit

extension UIFont {

    static func named(_ name: String, forTextStyle textStyle: TextStyle) -> UIFont {
        let preferredDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: textStyle)
        let descriptor = UIFontDescriptor(name: name, size: preferredDescriptor.pointSize)
        return UIFont(descriptor: descriptor, size: preferredDescriptor.pointSize)
    }

}
