//
//  String+HTML.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 16/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation

extension String {
    var strippingTags: String {
        return self
            .replacingOccurrences(of: "</p>(\\s|<[^>]+>)*<p>", with: "\n\n", options: .regularExpression, range: nil)
            .replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}
