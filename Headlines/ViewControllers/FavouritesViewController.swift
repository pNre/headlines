//
//  FavouritesViewController.swift
//  Headlines
//
//  Created by Joshua Garnham on 09/05/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import RealmSwift
import SDWebImage
import UIKit

class FavouritesViewController: UIViewController {

    @IBOutlet var tableView: UITableView!

    private lazy var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.dimsBackgroundDuringPresentation = false
        controller.searchResultsUpdater = self
        return controller
    }()

    private(set) weak var delegate: FavouritesViewControllerDelegate?
    private var articlesToken: NotificationToken?
    private var articles: Results<Article>? {
        didSet {
            title = "\(articles?.count ?? 0) favourites"
        }
    }

    init(delegate: FavouritesViewControllerDelegate?) {
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        articlesToken?.invalidate()
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        definesPresentationContext = true

        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "FavouriteCell", bundle: nil),
                           forCellReuseIdentifier: FavouriteCell.reuseIdentifier)

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(doneButtonPressed))

        if #available(iOS 11, *) {
            navigationItem.searchController = searchController
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }

        observeArticles()

    }

}

extension FavouritesViewController {
    @objc func doneButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
}

extension FavouritesViewController {
    private func observeArticles(filteringBy searchQuery: String? = nil) {
        articlesToken?.invalidate()
        articlesToken = ArticlesSource
            .favourites(filteringBy: searchQuery)
            .observe { [weak self] (change) in
                switch change {
                case .initial(let collection),
                     .update(let collection, _, _, _):
                    self?.articles = collection
                    self?.tableView?.reloadData()
                case .error:
                    break
                }
            }
    }
}

extension FavouritesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        observeArticles(filteringBy: searchController.searchBar.text)
    }
}

extension FavouritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FavouriteCell.reuseIdentifier,
                                                 for: indexPath) as! FavouriteCell
        cell.viewModel = FavouriteViewModel(article: articles![indexPath.row])
        return cell
    }
}

extension FavouritesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let article = articles?[indexPath.row] {
            delegate?.favouritesViewController(self, didSelect: article)
        }
    }
}

protocol FavouritesViewControllerDelegate: class {
    func favouritesViewController(_ viewController: FavouritesViewController, didSelect article: Article)
}
