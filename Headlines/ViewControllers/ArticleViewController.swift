//
//  ArticleViewController.swift
//  Headlines
//
//  Created by Joshua Garnham on 09/05/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleViewController: UIViewController {
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var headlineLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel!
    @IBOutlet var starButton: UIButton!

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override var prefersStatusBarHidden: Bool {
        return isContentOffsetBeyondHeadline
    }

    var article: Article {
        return viewModel.article
    }

    private(set) weak var delegate: ArticleViewControllerDelegate?
    private let viewModel: ArticleViewModel
    private var isContentOffsetBeyondHeadline = false {
        didSet {
            guard oldValue != isContentOffsetBeyondHeadline else {
                return
            }

            UIView.animate(withDuration: Constants.UI.animationDuration,
                           animations: setNeedsStatusBarAppearanceUpdate)
        }
    }

    init(article: Article, delegate: ArticleViewControllerDelegate?) {
        self.viewModel = ArticleViewModel(article: article)
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews(animated: false)
        viewModel.articleReloaded = { [weak self] in
            self?.configureViews(animated: true)
        }
    }

    private func configureViews(animated: Bool) {
        headlineLabel.attributedText = viewModel.headlineText
        bodyLabel.attributedText = viewModel.bodyText
        imageView.sd_setImage(with: viewModel.imageURL)
        scrollView.layoutIfNeeded()

        let duration = animated ? Constants.UI.animationDuration : 0
        UIView.transition(with: starButton, duration: duration, options: .transitionCrossDissolve, animations: {
            self.starButton.setBackgroundImage(self.viewModel.startButtonImage, for: .normal)
        })
    }
}

extension ArticleViewController {
    @IBAction func favouritesButtonPressed() {
        let favouritesViewController = FavouritesViewController(delegate: self)
        let navigationController = UINavigationController(rootViewController: favouritesViewController)
        present(navigationController, animated: true, completion: nil)
    }

    @IBAction func starButtonPressed() {
        ArticlesSource.update(article) {
            $0.toggleIsFavourite()
        }
    }
}

extension ArticleViewController: FavouritesViewControllerDelegate {
    func favouritesViewController(_ viewController: FavouritesViewController, didSelect article: Article) {
        viewController.presentingViewController?.dismiss(animated: true)
        delegate?.articleViewController(self, didSelect: article)
    }
}

extension ArticleViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentInset: CGFloat
        if #available(iOS 11, *) {
            contentInset = scrollView.adjustedContentInset.top
        } else {
            contentInset = scrollView.contentInset.top
        }

        isContentOffsetBeyondHeadline = scrollView.contentOffset.y + contentInset > headlineLabel.frame.minY
    }
}

protocol ArticleViewControllerDelegate: class {
    func articleViewController(_ viewController: ArticleViewController, didSelect favourite: Article)
}
