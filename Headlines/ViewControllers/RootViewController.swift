//
//  RootViewController.swift
//  Headlines
//
//  Created by Pierluigi D'Andrea on 16/03/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Alamofire
import RealmSwift
import UIKit

class RootViewController: UIPageViewController {

    override var childForStatusBarStyle: UIViewController? {
        return viewControllers?.first
    }

    override var childForStatusBarHidden: UIViewController? {
        return viewControllers?.first
    }

    private var articlesToken: NotificationToken?
    private var articles: Results<Article>?
    private var fetchArticlesRequest: Request?

    private var currentArticleViewController: ArticleViewController? {
        return viewControllers?.first(where: { $0 is ArticleViewController }) as? ArticleViewController
    }

    private var currentArticle: Article? {
        return currentArticleViewController?.article
    }

    init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    deinit {
        fetchArticlesRequest?.cancel()
        articlesToken?.invalidate()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        dataSource = self
        delegate = self
        observeArticles()
        reloadArticles()
    }

    private func reloadArticles() {
        fetchArticlesRequest?.cancel()
        fetchArticlesRequest = ArticlesService.fetchArticles(completion: { [weak self] articles, error in
            if let articles = articles {
                ArticlesSource.save(articles)
            } else if let error = error {
                self?.handleFetchArticlesError(error)
            }
        })
    }

    private func reloadViewControllers(currentViewController: UIViewController? = nil, animated: Bool) {
        let controller = currentViewController
            ?? articles?.first.map { ArticleViewController(article: $0, delegate: self) }
            ?? LoadingViewController()

        setViewControllers([controller], direction: .forward, animated: animated) { [weak self] _ in
            UIView.animate(withDuration: Constants.UI.animationDuration) {
                self?.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }

    private func selectViewController(for article: Article) {
        guard currentArticle?.isSameObject(as: article) != true else {
            return
        }

        let controller = ArticleViewController(article: article, delegate: self)
        reloadViewControllers(currentViewController: controller, animated: true)
    }

}

extension RootViewController {
    private func observeArticles() {
        articlesToken?.invalidate()
        articlesToken = ArticlesSource
            .all()
            .observe { [weak self] (change) in
                switch change {
                case .initial(let collection):
                    self?.articles = collection
                    self?.reloadViewControllers(animated: true)
                case .update:
                    self?.reloadViewControllers(currentViewController: self?.currentArticleViewController,
                                                animated: true)
                case .error:
                    break
                }
            }
    }
}

extension RootViewController {
    private func handleFetchArticlesError(_ error: Error) {
        let message: String?
        if let error = error as? LocalizedError {
            message = error.errorDescription
        } else {
            message = (error as NSError).localizedDescription
        }

        let alertController = UIAlertController(title: "Error downloading articles",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: "OK",
                                        style: .cancel))
        alertController.addAction(.init(title: "Retry",
                                        style: .default,
                                        handler: { [weak self] _ in self?.reloadArticles() }))

        present(alertController, animated: true)
    }
}

extension RootViewController: ArticleViewControllerDelegate {
    func articleViewController(_ viewController: ArticleViewController, didSelect favourite: Article) {
        selectViewController(for: favourite)
    }
}

extension RootViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let articles = articles, let article = (viewController as? ArticleViewController)?.article else {
            return nil
        }

        guard let index = articles.index(of: article),
            let previousIndex = articles.index(index, offsetBy: -1, limitedBy: articles.startIndex) else {
            return nil
        }

        return ArticleViewController(article: articles[previousIndex], delegate: self)
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let articles = articles, let article = (viewController as? ArticleViewController)?.article else {
            return nil
        }

        let lastIndex = articles.index(before: articles.endIndex)
        guard let index = articles.index(of: article),
            let followingIndex = articles.index(index, offsetBy: 1, limitedBy: lastIndex) else {
            return nil
        }

        return ArticleViewController(article: articles[followingIndex], delegate: self)
    }

}

extension RootViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        UIView.animate(withDuration: Constants.UI.animationDuration,
                       animations: setNeedsStatusBarAppearanceUpdate)
    }
}
