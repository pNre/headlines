### What were your priorities, and why?

The main priority was to have an usable product that was as close as possible to the requirements.

To do so I, in order:

- Migrated the project to Swift 4.2. On the long run Swift 3 could have become a technical debt, and given that at the beginning there was very little code, the migration was painless and quick.
- Completed a working implementation of the UI.
- Implemented the navigation between the articles.
- Implemented favouriting.

At that point the core elements of the app were ready and I spent the time I had left polishing the result.

### If you had another two days, what would you have tackled next?

I would have:

- Added support for some form of pagination for the list of articles.
- Improved error handling.
- Improved support for Dynamic Type.
- Used a `UITextView` instead of `UILabel` for the articles body.
- Parsed the HTML in the article body, instead of stripping it, to preserve links and formatting.
- Customized the articles paging animation to match to one in th mockup.
- Added a refreshing mechanism to the list of articles.
- Used a CAShapeLayer to draw the "star" button and improved the animation.
- Updated the structure of some parts of the codebase, as per the next answer.

### What would you change about the structure of the code?

I would:

- Build an abstraction over Realm.
- Refactor the network layer to a more structured implementation (maybe using URLSession instead of Alamofire given how simple the requests are).
- Introduce a coordinator to handle presentation and dismissal of view controllers.

### What bugs did you find but not fix?

- The String extension to strip HTML tags fails for certain inputs (eg `<span class="<abc>">`).
- In case the request to fetch the articles fails and the user dismisses the error alert, that request is not performed again until the app is relaunched.

### What would you change about the visual design of the app?

I would:

- Change the location of the "star" and "favourites" buttons. I would probably move them in a toolbar in order to always have them at hand without having to scroll through the article.
- Add an indicator, like a `UIPageControl`, to highlight the presence of more then one article.
- Add a swipe gesture to quickly remove favorites directly from the list.
- Change the typeface of the headline. The one used in the mockup (_Superclarendon_) is not available on iOS ≥ 10.
- Add a list representation of the articles.
